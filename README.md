# Comment Section

Allows adding, editing, filtering comments.

## Development server

Run `ng serve` for a dev server.Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Data

Located in assets/data.json

Comment Format:
```
{
  "id": 1,
  "title": "This is an item",
  "text": "This is a <b>bold description</b> of the item, it might describe a bug/task/comment, it can also display <a href='http://google.com'>Links</a>",
  "tags": ["test"]
}
```

## Dependencies

Angular Material

Angular Forms