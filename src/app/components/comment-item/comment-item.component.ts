import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { Comment } from '../../models/Comment';
import { DataService } from '../../services/data.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips'
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})
export class CommentItemComponent implements OnInit {

  @Input() comment: Comment;
  @Input() allTags: Array<string>;

  @Output() updateComment: EventEmitter<Comment> = new EventEmitter;
  @Output() deleteComment: EventEmitter<Comment> = new EventEmitter;

  public isEditing: boolean = false;
  public cacheComment: Comment;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<string[]>;
  tags: Array<string> = [];

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private commentService: DataService) { 
    
  }

  ngOnInit(): void {
    this.cacheComment = JSON.parse(JSON.stringify(this.comment));
    this.filteredTags = this.tagCtrl.valueChanges.pipe(
      startWith(this.comment.tags),
      map((tag: string | null) => this.allTags.slice()));
  }

  onEditClick(comment: Comment): void{
    this.isEditing = !this.isEditing;
  }

  onSaveClick(comment: Comment): void{
    this.comment = JSON.parse(JSON.stringify(comment));
    this.cacheComment = JSON.parse(JSON.stringify(this.comment));
    this.updateComment.emit(comment);
    this.isEditing = !this.isEditing;
  }

  onTagAdd(event: any): void {
    const input = event.input;
    const value = event.value;

    if((value || '').trim() && !this.comment.tags.includes(value.trim())) {
      this.comment.tags.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  onTagRemove(tag: string): void {
    const index = this.comment.tags.indexOf(tag);

    if (index >= 0) {
      this.comment.tags.splice(index, 1);
    }
  }

  onCancelClick(comment: Comment): void {
    this.comment = JSON.parse(JSON.stringify(this.cacheComment));
    this.isEditing = !this.isEditing;
  }

  onDeleteClick(comment: Comment): void{
    this.deleteComment.emit(comment);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.comment.tags.push(event.option.viewValue);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }
}
