import { Component, OnInit } from '@angular/core';
import { Comment } from '../../models/Comment';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {

  comments: Array<Comment>;
  tags: Array<string>;
  selectedTag: string = 'all';

  constructor(private commentService: DataService) { }

  ngOnInit(): void {
    this.commentService.getComments().subscribe(comments => {
      this.comments = comments;
      this.tags = this.getAllTags();
    });
  }

  getAllTags(): Array<string>{
    if(!this.comments) return [];
    const tags:Set<string> = new Set();
    for(let comment of this.comments){
      for(let tag of comment.tags) tags.add(tag);
    }
    return Array.from(tags);
  }

  filterByTag(comments: Comment[], selectedTag: string): Comment[]{
    if(selectedTag === 'all') return this.comments;
    
    return this.comments.filter((comment) => comment.tags.includes(selectedTag));
  }

  deleteComment(comment: Comment): void{
    this.comments = this.comments.filter(t => t.id !== comment.id);
    this.tags = this.getAllTags();
  }

  updateComment(comment: Comment): void{
    this.comments = this.comments.map((c) => {
      if(comment.id === c.id){
        c = comment;
      }
      return c;
    })
    this.tags = this.getAllTags();
  }

  addComment(comment: Comment){
    this.comments.push(comment);
    this.tags = this.getAllTags();
  }
}
