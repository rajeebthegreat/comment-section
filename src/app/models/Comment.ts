export class Comment{
  id?: number;
  title: string;
  text: string;
  tags: Array<string> = [];
}